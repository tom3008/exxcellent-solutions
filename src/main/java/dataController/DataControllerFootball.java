package dataController;

import java.util.LinkedList;

import de.exxcellent.challenge.csvReader.CsvReader;

/**
 * This class is aimed to handle the data from the Football CSV file and deliver the desired results. 
 * In this Case only the name of the team with the smallest absolute difference between "Goals" and "Allowed Goals"
 * 
 * @author Tobias Joechle
 *
 */

public class DataControllerFootball {
	
	//Constructor
	public DataControllerFootball() {
		
	}
	
	
	/**
	 * 
	 * @return Returns the name (as an String) of the team with the smallest absolute difference between "Goals" and "Allowed Goals"
	 * 		   Returns an empty String if there are no CSV data
	 */
	public String smallestGoalDifference () {
		String teamName = "";
		int minimumDistance = 0;
		int actualDistance = 0;
		String[] actualDataLine;
		
		CsvReader csvReader = new CsvReader();
		LinkedList<String[]> csvData = csvReader.ReadCsvFootball();
		
		//remove the first line from the CSV File out of the list, because it contains the names of the columns
		if (!csvData.isEmpty()) {
			csvData.removeFirst();
		}
		else {
			return teamName;
		}
		
		//Load the first Data
		if (!csvData.isEmpty()) {
			actualDataLine = csvData.poll();
			teamName = actualDataLine[0];
			minimumDistance = Math.abs(Integer.parseInt(actualDataLine[5])-Integer.parseInt(actualDataLine[6]));
		}
		else {
			return teamName;
		}
		
		//Find the team with the smallest absolute distance between "Goals" and "Goals allowed
		while (!csvData.isEmpty()) {
			actualDataLine = csvData.poll();
			actualDistance = Math.abs(Integer.parseInt(actualDataLine[5])-Integer.parseInt(actualDataLine[6]));
			
			if(minimumDistance > actualDistance) {
				teamName = actualDataLine[0];
				minimumDistance = actualDistance;
			}
		}
		
		
		return teamName;
	}

}
