package dataController;

import java.util.LinkedList;

import de.exxcellent.challenge.csvReader.CsvReader;

/**
 * This class is aimed to handle the data from the Weather CSV file and deliver the desired results. 
 * In this Case only the day number of the day with the smallest temperature spread
 *
 * @author Tobias Joechle
 *
 */


public class DataControllerWeather {
	
	
	//Constructor
	public DataControllerWeather() {
		
	}
	
	/**
	 * 
	 * @return Returns the day number (as an int) of the day with the smallest temperature spread.
	 * 		   Returns 0 if there are no CSV data.
	 */
	public int minimumTempSpread() {
		
		int minimumSpread = 0;
		int actualSpread;
		int dayNumber = 0;
		String[] actualDataLine;
		CsvReader csvReader = new CsvReader();
		LinkedList<String[]> csvData = csvReader.ReadCsvWeather();
		
		
		//remove the first line from the CSV File out of the list, because it contains the names of the columns
		if (!csvData.isEmpty()) {
			csvData.removeFirst();
		}
		else {
			return dayNumber;
		}
		
		//Load the first data
		if (!csvData.isEmpty()) {
			actualDataLine = csvData.pollFirst();
			dayNumber = Integer.parseInt(actualDataLine[0]);
			minimumSpread = Integer.parseInt(actualDataLine[1]) - Integer.parseInt(actualDataLine[2]);
		}
		else {
			return dayNumber;
		}
		
		//Find the day with the minimum temperature spread
		while (!csvData.isEmpty()) {
			actualDataLine = csvData.pollFirst();
			actualSpread = Integer.parseInt(actualDataLine[1]) - Integer.parseInt(actualDataLine[2]);
			
			if (minimumSpread > actualSpread) {
				dayNumber = Integer.parseInt(actualDataLine[0]);
				minimumSpread = actualSpread;
			}
		}
		
		return dayNumber;
	}
	

}
