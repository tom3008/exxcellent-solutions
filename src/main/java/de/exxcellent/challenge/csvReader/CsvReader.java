package de.exxcellent.challenge.csvReader;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;

/**
 * 
 * This class reads the desired CSV file and return the content in a Linked List with an Array for each split line
 * 
 * @author Tobias Joechle
 *
 */


public class CsvReader {
	
	//Constructor
	public CsvReader () {
		
	}

	/**
	 * This Method reads the weather.csv file, split it by a "," and returns a Linked List with an Array for each split line
	 * @return A Linked List with an Array for each split line of the Weather CSV file
	 */
	
	public LinkedList<String[]> ReadCsvWeather() {
		return ReadCsv ("de/exxcellent/challenge/weather.csv");
		
	}
	
	
	/**
	 * This Method reads the football.csv, split it by a "," and returns a Linked List with an Array for each split line
	 * @return A Linked List with an Array for each split line of the F CSV file
	 */
	public LinkedList<String[]> ReadCsvFootball() {
		return ReadCsv ("de/exxcellent/challenge/football.csv");
	}
	
	
	/**
	 * This Method reads a CSV file, split it by a "," and returns a Linked List with an Array for each split line
	 * @param filePath
	 * @return a Linked List with an Array for each split line
	 */
	public LinkedList<String[]> ReadCsv (String filePath) {
		
		String csvFile = filePath;
		String line ="";
		String cvsSplitBy = ",";
		
		LinkedList<String[]> resultData = new LinkedList<>();
		
		
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(this.getClass().getResourceAsStream("/" + filePath)));
			
			while ((line = br.readLine()) != null) {
				
				String[] FileLine = line.split(cvsSplitBy);
				resultData.add(FileLine);	
			}
			br.close();
						
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return resultData;
		
	}
	
	
	
	
	
	
}
