package de.exxcellent.challenge;

import dataController.DataControllerFootball;
import dataController.DataControllerWeather;

/**
 * The entry class for your solution. This class is only aimed as starting point and not intended as baseline for your software
 * design. Read: create your own classes and packages as appropriate.
 *
 * @author Benjamin Schmid <benjamin.schmid@exxcellent.de> edited by Tobias Joechle
 */
public final class App {

    /**
     * This is the main entry method of your program.
     * 
     * There are two option to start this program.
     * The first one is without CLI arguments, this will deliver once the Day with the smallest temperature spread
     * and once the team with the smalles goal spread.
     * 
     * The second option is to use the arguments "weather" and / or "football"
     * 
     * @param args The CLI arguments passed
     */
    public static void main(String... args) {
    	
    	// Your preparation code …
    	
    	if (args.length == 0) {
    		DataControllerWeather weatherData = new DataControllerWeather();
            int dayWithSmallestTempSpread = weatherData.minimumTempSpread();     // Your day analysis function call …
            System.out.printf("Day with smallest temperature spread : %s%n", dayWithSmallestTempSpread);

            DataControllerFootball footballData = new DataControllerFootball();
            String teamWithSmallestGoalSpread = footballData.smallestGoalDifference(); // Your goal analysis function call …
            System.out.printf("Team with smallest goal spread       : %s%n", teamWithSmallestGoalSpread);
    	}
    	else {
    		for (int i = 0; i < args.length; i++) {
				switch (args[i]) {
					case "weather":
						DataControllerWeather weatherData = new DataControllerWeather();
				        int dayWithSmallestTempSpread = weatherData.minimumTempSpread();     // Your day analysis function call …
				        System.out.printf("Day with smallest temperature spread : %s%n", dayWithSmallestTempSpread);
				        break;
					case "football":
						 DataControllerFootball footballData = new DataControllerFootball();
					     String teamWithSmallestGoalSpread = footballData.smallestGoalDifference(); // Your goal analysis function call …
					     System.out.printf("Team with smallest goal spread       : %s%n", teamWithSmallestGoalSpread);
					     break;
					default:
						System.out.println("You can only use the arguments 'weather' or 'football' or leave the argument enpty for both resulsts");
						break;
				}
    		}
    	}
    	

    	

       
    }
}
