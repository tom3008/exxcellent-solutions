package de.exxcellent.challenge;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import dataController.DataControllerWeather;

/**
 * Test if the result of the weatherchallenge is correct
 * 
 * @author Tobias Joechle
 *
 */

class WeatherTest {
	DataControllerWeather wetherController = new DataControllerWeather();
	@Test
	void test() {
		assertEquals(14, wetherController.minimumTempSpread());
	}

}
