package de.exxcellent.challenge;

import static org.junit.jupiter.api.Assertions.*;

import java.util.LinkedList;

import org.junit.jupiter.api.Test;

import de.exxcellent.challenge.csvReader.CsvReader;


/**
 * Test cases for the CsvReader
 * 
 * @author Tobias Joechle
 *
 */

class CsvReaderTest {

	@Test
	void test() {
		CsvReader testCsvReader = new CsvReader();
		LinkedList<String[]> csvDataWeather = testCsvReader.ReadCsvWeather();
	
		
		//test if csvDataWeather has the full 31 Elements
		assertEquals(31, csvDataWeather.size());
		
		
		LinkedList<String[]> csvDataFootball = testCsvReader.ReadCsvFootball();
		
		//test if csvDataFootball has the full 21 Elements
		assertEquals(21, csvDataFootball.size());
	}

}
