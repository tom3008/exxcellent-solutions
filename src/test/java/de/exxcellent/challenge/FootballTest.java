package de.exxcellent.challenge;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import dataController.DataControllerFootball;


/**
 * Test if the result of the footballchallenge is correct
 * 
 * @author Tobias Joechle
 *
 */

class FootballTest {
	DataControllerFootball footballController = new DataControllerFootball();
	
	@Test
	void test() {
		assertEquals("Aston_Villa", footballController.smallestGoalDifference());
	}

}
